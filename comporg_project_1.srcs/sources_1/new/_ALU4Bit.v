module ALU4Bit(a, b, less, cin, op, result, cout, g, p, zero, set);
input [3:0]a;
input [3:0]b;
input less;
input cin;
input [2:0]op;

output [3:0]result;
output cout;
output g;
output p;
output zero;
output set;

assign result = {result1, result2, result3, result4};
assign cout = {cout0, cout1, cout2, cout3, cout4};
assign g = {g1, g2, g3, g4};
assign p = {p1, p2, p3, p4};
assign zero = zero1 & zero2 & zero3 & zero4;
assign set = set1;

wire result1, cout1, g1, p1, zero1, set1;
ALU1Bit get_res_1(a[3], b[3], less, cin, op, result1, cout1, g1, p1, zero1, set1);  //we'll ignore the cin/cout here since we're using the cla for that
wire result2, cout2, g2, p2, zero2, set2;
ALU1Bit get_res_2(a[2], b[2], less, cin, op, result2, cout2, g2, p2, zero2, set2);  //we'll ignore the cin/cout here since we're using the cla for that
wire result3, cout3, g3, p3, zero3, set3;
ALU1Bit get_res_3(a[1], b[1], less, cin, op, result3, cout3, g3, p3, zero3, set3);  //we'll ignore the cin/cout here since we're using the cla for that
wire result4, cout4, g4, p4, zero4, set4;
ALU1Bit get_res_4(a[0], b[0], less, cin, op, result4, cout4, g4, p4, zero4, set4);  //we'll ignore the cin/cout here since we're using the cla for that

wire cout0;
wire [3:0]sum;
cla add(a, b, p, g, cin, sum, cout0);  //we pass the calculated p, g here since it uses signed_b

if (op[1:0] == 2'b10) begin
	result = sum;
	cout = cout0;
end


wire binv = op[2];
wire signed_b = b & ~binv;

wire and_res = a & signed_b;
wire or_res = a | signed_b;
wire sum = (a ^ signed_b ^ cin) | (a & signed_b & cin);  //sum is 1 if either just one bit or all bits are 1's
wire cout = (~a ^ ~signed_b ^ ~cin) | (a & signed_b & cin);  //cout is 1 if either just one bit is 0 or all bits are 1's
wire slt = (~a & signed_b) | less;  //if (0, 1 == a, b), then a is less than b

wire mux_output;

//and: 0, or: 1, sum:2, slt: 3
mux1bit4to1 result_selector(and_res, or_res, sum, slt, op[1:0], mux_output);
assign result = mux_output;
assign zero = ~result;
assign p = or_res;
assign g = and_res;

endmodule
