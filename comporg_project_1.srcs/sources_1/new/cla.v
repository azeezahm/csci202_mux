`timescale 1ns / 1ps

module cla(a, b, p, g, cin, sum, cout);

input [3:0] a;
input [3:0] b;
input [3:0] p;
input [3:0] g;
input cin;
output [3:0] sum;
output cout;

wire [4:0] carry_bits;

cla_block get_carry_bits(p, g, cin, carry_bits);

assign sum = p[3:0] ^ carry_bits[3:0];
assign cout = carry_bits[4];

endmodule
