`timescale 1ns / 1ps

module ALU1Bit(a, b, less, cin, op, result, cout, g, p, zero, set);
input a;
input b;
input less;
input cin;
input [2:0]op;

output result;
output cout;
output g;
output p;
output zero;
output set;

wire signed_b;
assign signed_b = b & ~op[2];
assign p = a | signed_b;
assign g = a & signed_b;

assign cout = g | (p & cin);

wire and_res = g;
wire or_res = p;
wire sum = (a ^ signed_b ^ cin) | (a & signed_b & cin);  //sum is 1 if either just one bit or all bits are 1's
wire slt = less;

//and: 0, or: 1, sum:2, slt: 3
mux1bit4to1 result_selector(and_res, or_res, sum, slt, op[1:0], result);
assign zero = ~result;
assign set = result;
endmodule