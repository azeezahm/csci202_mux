`timescale 1ns / 1ps

module ALU4Bit(a, b, less, cin, op, result, cout, G, P, zero, set);
input [3:0] a, b;
input less, cin;
input [2:0] op;

output [3:0] result, G, P;
output cout;
output zero;
output set;  //msb of result

wire [4:0] c;
wire [3:0] zeroes;
wire dontcare;
wire binv = op[2];

assign zero = zeroes[3] & zeroes[2] & zeroes[1] & zeroes[0];
assign cout = c[4];

ALU1Bit get_res_1(a[3], b[3], less, c[3], op, result[3], dontcare, G[3], P[3], zeroes[3], set);
ALU1Bit get_res_2(a[2], b[2], less, c[2], op, result[2], dontcare, G[2], P[2], zeroes[2], dontcare);
ALU1Bit get_res_3(a[1], b[1], less, c[1], op, result[1], dontcare, G[1], P[1], zeroes[1], dontcare);
ALU1Bit get_res_4(a[0], b[0], less, c[0], op, result[0], dontcare, G[0], P[0], zeroes[0], dontcare);

cla_block get_carry_bits(P, G, cin, c);

endmodule
