`timescale 1ns / 1ps

module cla_block(p, g, cin, carry_bits);
input [3:0] p, g;
input cin;
output [4:0] carry_bits;

//c0 = cin
assign carry_bits[0] = cin;
//c1 = g0 + (p0*c0)
assign carry_bits[1] = g[0] | (p[0] & cin);
//c2 = g1 + (p1*g0) + (p1*p0*c0)
assign carry_bits[2] = g[1] | (p[1] & g[0]) | (p[1] & p[0] & cin);
//c3 = g2 + (p2*g1) + (p2*p1*g0)+(p2*p1*p0*c0)
assign carry_bits[3] = g[2] | (p[2] & g[1]) | (p[2] & p[1] & g[0]) | (p[2] & p[1] & p[0] & cin);
//c4 = g3 + (p3*g2) + (p3*p2*g1) + (p3*p2*p1*g0) + (p3*p2*p1*p0*c0) 
assign carry_bits[4] = g[3] | (p[3] & g[2]) | (p[3] & p[2] & g[1]) | 
            (p[3] & p[2] & p[1] & g[0]) | (p[3] & p[2] & p[1] & p[0] & cin);    
endmodule
