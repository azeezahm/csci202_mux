module mux1bit4to1(a, b, c, d, op, result);
input a, b, c, d;
input [1:0]op;
output result;

wire upper, lower;
wire mux_result;
mux1bit2to1 choose_upper(a, b, op[0], upper);
mux1bit2to1 choose_lower(c, d, op[0], lower);
mux1bit2to1 choose_result(upper, lower, op[1], mux_result);

assign result = mux_result;

endmodule
