`timescale 1ns / 1ps

module tb_ALU1Bit();
    reg a, b;
    reg [2:0] op;
    reg cin, less;
    wire result;
    wire cout;
    wire g;
    wire p;
    wire zero;
    integer i, j;
    
    ALU1Bit DUT (.a(a), .b(b), .less(less), .cin(cin), .op(op), .result(result), .cout(cout), .g(g), .p(p), .zero(zero));
    
    initial begin
        a = 0;
        b = 0;
        cin = 0;
        less = 0;
        
        //test and
        op = 3'b000;
        for (i = 0; i < 2; i = i+1) begin
            a = i;
            for (j = 0; j < 2; j = j+1) begin
                b = j;
                #1;
            end
        end

        //test or
        op = 3'b001;
        for (i = 0; i < 2; i = i+1) begin
            a = i;
            for (j = 0; j < 2; j = j+1) begin
                b = j;
                #1;
            end
        end

        //test add
        op = 3'b010;
        for (i = 0; i < 2; i = i+1) begin
            a = i;
            for (j = 0; j < 2; j = j+1) begin
                b = j;
                #1;
            end
        end
        
        //test slt
        op = 3'b111;
        less = 1;
        for (i = 0; i < 2; i = i+1) begin
            a = i;
            for (j = 0; j < 2; j = j+1) begin
                b = j;
                #1;
            end
        end
    end
endmodule
