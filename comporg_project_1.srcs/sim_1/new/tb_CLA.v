`timescale 1ns / 1ps

module tb_CLA();
    reg [3:0] a, b;
    wire [3:0] p, g;
    reg cin;
    wire [3:0]sum;
    wire cout;
    integer i, j;
    
    assign p = a | b;
    assign g = a & b;
    
    cla DUT (.a(a), .b(b), .p(p), .g(g), .sum(sum), .cin(cin), .cout(cout));
    
    initial begin
        a = 0;
        b = 0;
        cin = 1;
    
        for (i = 0; i < 16; i = i+1) begin
            a = i;
            for (j = 0; j < 16; j = j+1) begin
                b = j;
                #1;
            end
        end
    end
endmodule
